import React from "react";
import { Menu, Icon } from "antd";

const right = {
  float: "right"
};
// const left = {
//   float: "left"
// };

class Navigation extends React.Component {
  render() {
    return (
      <Menu mode="horizontal">
        <Menu.Item key="login" style={right}>
          <Icon type="login" /> Login
        </Menu.Item>
        <Menu.Item key="join" style={right}>
          <Icon type="fork" /> Join
        </Menu.Item>
      </Menu>
    );
  }
}
export default Navigation;
