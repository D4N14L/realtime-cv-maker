export const isUnique = (arr, index, toBeCompared) => {
  let result;
  if (!index) {
    result = arr.findIndex(
      predict => predict.toLowerCase() === toBeCompared.toLowerCase()
    );
  } else {
    result = arr.findIndex(
      predict => predict[index].toLowerCase() === toBeCompared.toLowerCase()
    );
  }
  return result === -1 ? true : false;
};
