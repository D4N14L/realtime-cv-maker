import React, { Component } from "react";
import { Input, Icon, message, Timeline } from "antd";
import { connect } from "react-redux";
import { AddInterest } from "../../actions";
import InterestTimeline from "./InterestTimeline";
// helpers
import { isUnique } from "../Helpers/isUnique";

// styles
const padding = { padding: "12px 24px" };
const bold = { fontWeight: "bold" };

class Interest extends Component {
  constructor() {
    super();
    this.state = {
      interest: null
    };
  }
  interestSubmitHandler = event => {
    event.preventDefault();
    if (this.state.interest.length > 0) {
      if (isUnique(this.props.interests, null, this.state.interest)) {
        this.props.dispatch(AddInterest(this.state.interest.trim()));
        message.success("Interest Added!");
        this.setState({ interest: "" });
      } else {
        message.error("Interest Already exists!");
      }
    }
  };
  inputStateHandler = event => {
    this.setState({ interest: event.target.value });
  };
  render() {
    return (
      <div style={padding}>
        <p style={bold}>Interests</p>
        <br />
        <Timeline>
          <InterestTimeline interests={this.props.interests} />
          <Timeline.Item
            dot={
              <Icon type="plus-circle" theme="twoTone" twoToneColor="orange" />
            }
          />
        </Timeline>

        <form onSubmit={this.interestSubmitHandler}>
          <center>
            <Input
              style={{ width: "70%" }}
              autoFocus
              placeholder="Add Interest"
              prefix={<Icon type="star" />}
              value={this.state.interest}
              onChange={this.inputStateHandler}
            />
          </center>
        </form>
      </div>
    );
  }
}
const mapStateToPros = state => {
  return {
    interests: state.Interests
  };
};
export default connect(mapStateToPros)(Interest);
