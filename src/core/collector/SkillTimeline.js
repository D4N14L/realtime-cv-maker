import React from "react";
import { Timeline, Button, Rate, message, Icon } from "antd";
import { connect } from "react-redux";
import { DeleteSkill, UpdateRateSkill, UpdateSkill } from "../../actions/";
import EditButton from "./partials/EditButton";
class SkillTimeline extends React.Component {
  // 1 - Update Skill Rating Handler

  ratingHandler = (value, index) => {
    this.props.dispatch(UpdateRateSkill(index, value));
    message.success(`Rating Updated For ${this.props.skills[index].name}`);
  };

  // 2 - Is skill update finished

  isEnterPressHandler = (index, value, event) => {
    if (event) {
      if (event.key === "Enter") {
        if (value.length > 0) {
          if (
            this.props.skills.findIndex(skills => skills.name === value) === -1
          ) {
            let Update = this.props.dispatch(UpdateSkill(index, value));
            if (Update.payload.name.toLowerCase() === value.toLowerCase()) {
              message.success(`Skill Name Updated`);
            } else {
              message.error(`Failed To Update ${Update.payload.name}`);
            }
            event.target.parentNode.parentNode.parentNode.nextSibling.firstChild.click();
          } else {
            message.error(`${value} Already Exists`);
          }
        }
      }
    } else {
      if (value.length > 0) {
        if (
          this.props.skills.findIndex(skills => skills.name === value) === -1
        ) {
          let Update = this.props.dispatch(UpdateSkill(index, value));
          if (Update.payload.name.toLowerCase() === value.toLowerCase()) {
            message.success(`Skill Name Updated`);
          } else {
            message.error(`Failed To Update ${Update.payload.name}`);
          }
        } else {
          message.error(`${value} Already Exists`);
        }
      }
    }
  };

  // 3 - Update Skill Name

  updateSkillName = () => {};

  // 4 - Delete Skill Handler

  deleteHandler = event => {
    this.props.dispatch(DeleteSkill(event.target.id.split("-")[2]));
    message.success("Skill Deleted!");
  };

  render() {
    if (this.props.skills.length <= 0) {
      return (
        <Timeline.Item
          color="red"
          dot={
            <Icon
              theme="twoTone"
              type="exclamation-circle"
              style={{ fontSize: "16px" }}
            />
          }
        >
          <span style={{ fontSize: "12px" }}>
            You Haven't Added Any Skill Yet, Press &nbsp;
            <Button
              type="primary"
              shape="circle"
              size="small"
              icon="plus"
              style={{ fontSize: "14px" }}
            />
            &nbsp; below to add skills
          </span>
        </Timeline.Item>
      );
    } else {
      return this.props.skills.map((skill, index) => (
        <Timeline.Item theme="twoTone" key={index}>
          {skill.name}
          <Button
            type="dashed"
            size="default"
            style={{ float: "right", color: "red", marginLeft: "5px" }}
            shape="circle"
            icon="delete"
            id={`delete-skill-${index}`}
            onClick={this.deleteHandler}
          />

          <EditButton
            onKeyPress={event =>
              this.isEnterPressHandler(index, event.target.value.trim(), event)
            }
            placeholder={skill.name}
            onConfirm={e => {
              let value =
                e.target.parentNode.parentNode.firstChild.childNodes[1]
                  .firstChild.firstChild.value;
              this.isEnterPressHandler(index, value, null);
            }}
          >
            <Button
              type="dashed"
              size="default"
              style={{ float: "right", color: "#40a9ff" }}
              shape="circle"
              icon="highlight"
            />
          </EditButton>

          <Rate
            style={{ float: "right", marginRight: "5px" }}
            tooltips={["Beginner", "Average", "Good", "Pro", "Pro +"]}
            onChange={value => this.ratingHandler(value, index)}
            value={skill.rating}
          />
        </Timeline.Item>
      ));
    }
  }
}

export default connect()(SkillTimeline);
