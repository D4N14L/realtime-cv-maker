import React from "react";
import { Timeline, Icon, Row, Col, Button, message } from "antd";
import { DeleteInterest, UpdateInterest } from "../../actions";
import { connect } from "react-redux";
import { isUnique } from "../Helpers/isUnique";
import EditButton from "./partials/EditButton";

class InterestTimeline extends React.Component {
  deleteHandler = index => {
    this.props.dispatch(DeleteInterest(index));
    message.success("Interest Deleted!");
  };

  isEnterPressHandler = (index, value, event) => {
    if (event) {
      if (event.key === "Enter") {
        if (value.length > 0) {
          if (isUnique(this.props.interests, null, value)) {
            let Update = this.props.dispatch(UpdateInterest(index, value));

            if (Update.payload.value.toLowerCase() === value.toLowerCase()) {
              message.success(`Interest Name Updated`);
            } else {
              message.error(`Failed to update the current interest`);
            }
            event.target.parentNode.parentNode.parentNode.nextSibling.firstChild.click();
          } else {
            message.error(`Interest Already Exists`);
          }
        }
      }
    } else {
      if (value.length > 0) {
        if (isUnique(this.props.interests, null, value)) {
          this.props.dispatch(UpdateInterest(index, value));
          let Update = this.props.dispatch(UpdateInterest(index, value));

          if (Update.payload.value.toLowerCase() === value.toLowerCase()) {
            message.success(`Interest Name Updated`);
          } else {
            message.error(`Failed to update the current interest`);
          }
        } else {
          message.error(`Interest Already Exists`);
        }
      }
    }
  };

  render() {
    if (this.props.interests.length <= 0) {
      return (
        <Timeline.Item dot={<Icon type="star" style={{ color: "red" }} />}>
          Lets Add Your Interests Here.
        </Timeline.Item>
      );
    }
    return this.props.interests.map((interest, index) => (
      <Timeline.Item
        style={{ wordWrap: "break-word", width: "auto" }}
        key={index}
        dot={<Icon type="star" theme="twoTone" twoToneColor="orange" />}
      >
        <Row>
          <Col span={20} xs={15} lg={20} md={20}>
            {interest}
          </Col>

          <Col span={4} xs={9} lg={4} md={4} style={{ display: "flex" }}>
            <EditButton
              onKeyPress={event =>
                this.isEnterPressHandler(
                  index,
                  event.target.value.trim(),
                  event
                )
              }
              placeholder={interest}
              onConfirm={e => {
                let value =
                  e.target.parentNode.parentNode.firstChild.childNodes[1]
                    .firstChild.firstChild.value;
                this.isEnterPressHandler(index, value, null);
              }}
            >
              <Button shape="circle" type="dashed" style={{ color: "#40a9ff" }}>
                <Icon type="highlight" />
              </Button>
            </EditButton>
            &nbsp;
            <Button
              shape="circle"
              onClick={event => this.deleteHandler(index)}
              type="dashed"
              style={{ color: "red" }}
            >
              <Icon type="delete" />
            </Button>
          </Col>
        </Row>
      </Timeline.Item>
    ));
  }
}
export default connect()(InterestTimeline);
