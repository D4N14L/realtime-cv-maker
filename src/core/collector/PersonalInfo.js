import React from "react";
import { connect } from "react-redux";
import { Row, Col, Input } from "antd";
import {
  FirstNameAction,
  MiddleNameAction,
  LastNameAction,
  DesignationAction,
  DescriptionAction,
  EmailAction,
  AddressAction,
  LinkedinAction
} from "../../actions";

const { TextArea } = Input;

// styles

const padding = { padding: "12px 24px" };
const margin = { marginTop: "1.5em" };

// actions

// Personal info component

class PersonalInfo extends React.Component {
  /*
    ******************
        Handlers
    ******************
             */

  // 1 - First Name Handler
  firstNameHandler = event => {
    this.props.dispatch(FirstNameAction(event.target.value.trim()));
  };

  // 2 - Middle Name Handler

  middleNameHandler = event => {
    this.props.dispatch(MiddleNameAction(event.target.value.trim()));
  };

  // 3 - Last Name Handler

  lastNameHandler = event => {
    this.props.dispatch(LastNameAction(event.target.value.trim()));
  };

  // 4 - Designation Handler

  desginationHandler = event => {
    this.props.dispatch(DesignationAction(event.target.value));
  };

  // 5 - Description Handler

  littleDescriptionHandler = event => {
    this.props.dispatch(DescriptionAction(event.target.value.trim()));
  };

  // 6 - Email Handler
  emailHandler = event => {
    this.props.dispatch(EmailAction(event.target.value.trim()));
  };

  // 7 - Address Handler
  addressHandler = event => {
    this.props.dispatch(AddressAction(event.target.value.trim()));
  };
  // 8 - LinkedInHandler

  linkedinHander = event => {
    this.props.dispatch(LinkedinAction(event.target.value.trim()));
  };

  render() {
    return (
      <div style={padding}>
        <p>Personal Information</p>
        <Row type="flex" justify="space-between">
          {/* First Name */}

          <Col style={margin} lg={{ span: 11 }} xs={{ span: 16 }}>
            <Input
              onChange={this.firstNameHandler}
              value={this.props.PersonalInfo.firstName}
              placeholder="First Name"
            />
          </Col>

          {/* Middle Name */}

          <Col style={margin} lg={{ span: 12 }} xs={{ span: 16 }}>
            <Input
              onChange={this.middleNameHandler}
              placeholder="Middle Name"
              value={this.props.PersonalInfo.middleName}
            />
          </Col>

          {/* Last Name */}

          <Col style={margin} lg={{ span: 11 }} xs={{ span: 16 }}>
            <Input
              onChange={this.lastNameHandler}
              value={this.props.PersonalInfo.lastName}
              placeholder="Last Name"
            />
          </Col>

          {/* Designation  */}

          <Col style={margin} lg={{ span: 12 }} xs={{ span: 16 }}>
            <Input
              onChange={this.desginationHandler}
              placeholder="Designation"
              value={this.props.PersonalInfo.designation}
            />
          </Col>

          {/* Address */}

          <Col style={margin} lg={{ span: 11 }} xs={{ span: 16 }}>
            <Input
              onChange={this.addressHandler}
              value={this.props.PersonalInfo.address}
              placeholder="Address"
            />
          </Col>

          {/* Email */}

          <Col style={margin} lg={{ span: 12 }} xs={{ span: 16 }}>
            <Input
              onChange={this.emailHandler}
              value={this.props.PersonalInfo.email}
              placeholder="Email"
            />
          </Col>

          {/* Linkedin */}

          <Col style={margin} lg={{ span: 11 }} xs={{ span: 16 }}>
            <Input
              onChange={this.linkedinHander}
              value={this.props.PersonalInfo.linkedin}
              placeholder="LinkedIn"
            />
          </Col>

          {/* Description Name */}

          <Col style={margin} lg={{ span: 24 }} xs={{ span: 24 }}>
            <p>Tell a bit about yourself</p>
            <TextArea
              autoresize={{ minRows: 3, maxRows: 6 }}
              rows={6}
              placeholder="Tell a bit about yourself"
              onChange={this.littleDescriptionHandler}
              value={this.props.PersonalInfo.description}
            />
          </Col>
        </Row>
      </div>
    );
  }
}
const mapStateToProps = state => {
  return {
    PersonalInfo: state.PersonalInfo
  };
};
export default connect(mapStateToProps)(PersonalInfo);
