import React from "react";

// * - Core

import PersonalInfo from "./PersonalInfo";
import Skills from "./Skills";
import Interests from "./Interest";
import { Collapse, Icon } from "antd";
const Panel = Collapse.Panel;
const IconSize = { fontSize: "16px" };
const customPanelStyle = {
  // background: "#fff",
  // borderRadius: 4,
  // marginBottom: 24,
  // border: 0,
  // overflow: "hidden"
};

export default class Collector extends React.Component {
  render() {
    return (
      <React.Fragment>
        <Collapse
          bordered={false}
          expandIcon={({ isActive }) => (
            <Icon type="caret-right" rotate={isActive ? 90 : 0} />
          )}
        >
          <Panel
            style={customPanelStyle}
            header={
              <span style={IconSize}>
                <Icon type="user" />
                &nbsp;&nbsp;
                <span>Personal Information</span>
              </span>
            }
          >
            <PersonalInfo />
          </Panel>
          <Panel
            style={customPanelStyle}
            header={
              <span style={IconSize}>
                <Icon type="tool" />
                &nbsp;&nbsp;
                <span>Skills Information</span>
              </span>
            }
          >
            <Skills />
          </Panel>
          <Panel
            style={customPanelStyle}
            header={
              <span style={IconSize}>
                <Icon type="star" />
                &nbsp;&nbsp;
                <span>Interests &amp; Hobbies</span>
              </span>
            }
          >
            <Interests />
          </Panel>
        </Collapse>
      </React.Fragment>
    );
  }
}
