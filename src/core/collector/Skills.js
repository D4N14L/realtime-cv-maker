import React from "react";
import { connect } from "react-redux";
import { Timeline, Button, Icon, Input, Popconfirm, message } from "antd";

import { AddSkill } from "../../actions/";
import SkillTimeline from "./SkillTimeline";

// styles
const padding = { padding: "12px 24px" };

// components

class Skills extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      skillInput: "",
      currentlyHoveredSkill: null
    };
  }

  // Handlers

  // 1 - Add Skill Input Handler
  SkillInputHandler = event => {
    this.setState({ skillInput: event.target.value });
  };

  // 2 - Add Skill Submit Handler
  addSkillHander = event => {
    event.preventDefault();
    if (this.state.skillInput.length > 0) {
      if (
        this.props.skills.findIndex(
          skill =>
            skill.name.toLocaleLowerCase() ===
            this.state.skillInput.toLocaleLowerCase()
        ) === -1
      ) {
        this.props.dispatch(AddSkill(this.state.skillInput.trim()));
        message.success(`${this.state.skillInput} Added Successfully!`);
      } else {
        message.error(`${this.state.skillInput} Already Exists`);
      }
      this.setState({ skillInput: "" });
    }
  };

  // Renderer

  render() {
    return (
      <div style={padding}>
        <p style={{ fontWeight: "bold" }}>Skills</p>
        <br />
        <Timeline>
          <SkillTimeline skills={this.props.skills} />
          <Timeline.Item
            dot={<Icon type="plus-circle" style={{ fontSize: "16px" }} />}
          />
        </Timeline>
        <center>
          <br />
          <Popconfirm
            placement="top"
            onConfirm={this.addSkillHander}
            title={
              <form onSubmit={this.addSkillHander}>
                <Input
                  autoFocus
                  placeholder="Skill Name"
                  onChange={this.SkillInputHandler}
                  value={this.state.skillInput}
                />
              </form>
            }
            icon={
              <Icon
                type="plus-circle"
                style={{
                  fontSize: "14px",
                  fontWeight: "bold",
                  color: "#40a9ff"
                }}
              />
            }
            okText="Add "
          >
            <Button type="primary" shape="circle" icon="plus" />
          </Popconfirm>
        </center>
      </div>
    );
  }
}
const mapStateToProps = state => {
  return {
    skills: state.Skills
  };
};
export default connect(mapStateToProps)(Skills);
