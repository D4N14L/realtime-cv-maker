import React from "react";
import { Popconfirm, Input, Icon } from "antd";
export default function EditButton(props) {
  return (
    <Popconfirm
      placement="top"
      onConfirm={props.onConfirm}
      title={
        <form onSubmit={e => e.preventDefault()}>
          <Input
            autoFocus
            onKeyPress={props.onKeyPress}
            placeholder={props.placeholder}
          />
        </form>
      }
      icon={
        <Icon
          type={props.icon ? props.icon : "highlight"}
          style={{
            fontSize: "14px",
            fontWeight: "bold",
            color: props.color ? props.color : "#40a9ff"
          }}
        />
      }
      okText="Update"
    >
      {props.children}
    </Popconfirm>
  );
}
