import React, { Component } from "react";
import Navigation from "./core/Navigation";
import Collector from "./core/collector/Collector";
import { Row, Col } from "antd";
import { Provider } from "react-redux";
import { store } from "./store";
class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <Navigation />
        <Row type="flex" justify="start">
          <Col span={12}>
            <Collector />
          </Col>
          <Col
            style={{
              border: "1px solid red",
              position: "fixed",
              right: "0px",
              top: "7.5%"
            }}
            span={12}
          />
        </Row>
      </Provider>
    );
  }
}

export default App;
