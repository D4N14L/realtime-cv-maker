export const interestReducer = (state = [], { payload, type }) => {
  let newState = state;
  switch (type) {
    case "ADD_INTEREST":
      return [...state, payload];
    case "DELETE_INTEREST":
      newState.splice(payload, 1);
      return [...newState];
    case "UPDATE_INTEREST":
      newState[payload.index] = payload.value;
      return [...newState];
    default:
      return [...state];
  }
};
