const initialState = {
  firstName: "",
  middleName: "",
  lastName: "",
  designation: "",
  description: "",
  email: "",
  address: "",
  linkedin: ""
};
export const PersonalInfoReducer = (
  state = initialState,
  { payload, type }
) => {
  switch (type) {
    case "UPDATE_FIRST_NAME":
      return { ...state, firstName: payload };
    case "UPDATE_MIDDLE_NAME":
      return { ...state, middleName: payload };
    case "UPDATE_LAST_NAME":
      return { ...state, lastName: payload };
    case "UPDATE_EMAIL":
      return { ...state, email: payload };
    case "UPDATE_DESIGNATION":
      return { ...state, designation: payload };
    case "UPDATE_DESCRIPTION":
      return { ...state, description: payload };
    case "UPDATE_ADDRESS":
      return { ...state, address: payload };
    case "UPDATE_LINKEDIN":
      return { ...state, linkedin: payload };
    default:
      return state;
  }
};
