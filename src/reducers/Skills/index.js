const initialState = [];
export const SkillsReducer = (state = initialState, { payload, type }) => {
  let newState = state;
  switch (type) {
    case "ADD_SKILL":
      return [...state, payload];
    case "UPDATE_SKILL":
      newState[payload.index].name = payload.name;
      return [...newState];
    case "DELETE_SKILL":
      newState.splice(payload, 1);
      return [...newState];
    case "UPDATE_RATE_SKILL":
      newState[payload.index].rating = payload.rating;
      return [...newState];
    default:
      return [...state];
  }
};
