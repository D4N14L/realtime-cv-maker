import { PersonalInfoReducer } from "./PersonalInfo";
import { SkillsReducer } from "./Skills";
import { interestReducer } from "./Interests";
import { combineReducers } from "redux";
export default combineReducers({
  PersonalInfo: PersonalInfoReducer,
  Skills: SkillsReducer,
  Interests: interestReducer
});
