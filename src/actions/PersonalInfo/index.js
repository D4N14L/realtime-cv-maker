export const firstNameAction = firstName => {
  return {
    type: "UPDATE_FIRST_NAME",
    payload: firstName
  };
};

export const middleNameAction = middlename => {
  return {
    type: "UPDATE_MIDDLE_NAME",
    payload: middlename
  };
};

export const lastNameAction = lastname => {
  return {
    type: "UPDATE_LAST_NAME",
    payload: lastname
  };
};

export const designationAction = designation => {
  return {
    type: "UPDATE_DESIGNATION",
    payload: designation
  };
};

export const descriptionAction = description => {
  return {
    type: "UPDATE_DESCRIPTION",
    payload: description
  };
};

export const addressAction = address => {
  return {
    type: "UPDATE_ADDRESS",
    payload: address
  };
};

export const emailAction = email => {
  return {
    type: "UPDATE_EMAIL",
    payload: email
  };
};

export const linkedinAction = linkedin => {
  return {
    type: "UPDATE_LINKEDIN",
    payload: linkedin
  };
};
