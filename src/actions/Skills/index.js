export const addSkill = skill => {
  return {
    type: "ADD_SKILL",
    payload: {
      name: skill,
      rating: 0
    }
  };
};

export const deleteSkill = index => {
  return {
    type: "DELETE_SKILL",
    payload: index
  };
};

export const updateSkill = (index, name) => {
  return {
    type: "UPDATE_SKILL",
    payload: {
      index,
      name
    }
  };
};

export const updateRateSkill = (index, value) => {
  return {
    type: "UPDATE_RATE_SKILL",
    payload: {
      index,
      rating: value
    }
  };
};
