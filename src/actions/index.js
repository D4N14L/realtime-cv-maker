import {
  firstNameAction,
  middleNameAction,
  lastNameAction,
  designationAction,
  descriptionAction,
  emailAction,
  addressAction,
  linkedinAction
} from "./PersonalInfo";

// * - Skills Import Actions

import { addSkill, deleteSkill, updateSkill, updateRateSkill } from "./Skills";

// * - Interest Import Actions

import { addInterest, deleteInterest, updateInterest } from "./Interests";

// * - Personal Information Actions

export const FirstNameAction = firstNameAction;
export const MiddleNameAction = middleNameAction;
export const LastNameAction = lastNameAction;
export const DesignationAction = designationAction;
export const DescriptionAction = descriptionAction;
export const EmailAction = emailAction;
export const AddressAction = addressAction;
export const LinkedinAction = linkedinAction;

// * - Skills Information Actions

export const AddSkill = addSkill;
export const DeleteSkill = deleteSkill;
export const UpdateSkill = updateSkill;
export const UpdateRateSkill = updateRateSkill;

// * - Interests Information Actions

export const AddInterest = addInterest;
export const DeleteInterest = deleteInterest;
export const UpdateInterest = updateInterest;
