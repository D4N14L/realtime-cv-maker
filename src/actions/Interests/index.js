export const addInterest = value => {
  return {
    type: "ADD_INTEREST",
    payload: value
  };
};

export const deleteInterest = index => {
  return {
    type: "DELETE_INTEREST",
    payload: index
  };
};

export const updateInterest = (index, value) => {
  return {
    type: "UPDATE_INTEREST",
    payload: {
      value,
      index
    }
  };
};
